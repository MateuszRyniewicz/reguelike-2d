﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public Rigidbody2D _rb;
    private Animator _anim;

    public float _speedEnemy;
    public int health = 150;
    public GameObject[] _deathSplatters;
    public GameObject hitEffect;

    public bool shouldShot;

    public GameObject bullet;
    public Transform firePoint;

    public float fireRate=0.2f;
    private float fireCounter;


    public float _rangeToChasePlayer;
    private Vector3 moveDirection;
   
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, PlayerController.Instance.transform.position) < _rangeToChasePlayer)
        {
            moveDirection = PlayerController.Instance.transform.position - transform.position;
        }
        else
        {
            moveDirection = Vector3.zero;
        }
        moveDirection.Normalize();

        _rb.velocity = moveDirection*_speedEnemy;







        if(moveDirection!= Vector3.zero)
        {
            _anim.SetBool("isMoving", true);
        }
        else
        {
            _anim.SetBool("isMoving", false);
        }

        if (shouldShot)
        {
            fireCounter -= Time.deltaTime;
           
            if (fireCounter <= 0)
            {
                fireCounter = fireRate;
                Instantiate(bullet, firePoint.position, firePoint.rotation);
            }
        }
            
    }

    public void DamageEnemy(int damage)
    {
        health -= damage;

        Instantiate(hitEffect, transform.position, Quaternion.identity);

        if (health <= 0)
        {
            Destroy(gameObject);

            int selletedSplatters =Random.Range(0,_deathSplatters.Length);
            int selletedRotation = Random.Range(0, 4);

            Instantiate(_deathSplatters[selletedSplatters], transform.position, Quaternion.Euler(0f,0f,selletedRotation*90));

          //  Instantiate(_deathSplatter, transform.position, Quaternion.identity);
        }
    }

   
}
