﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;

    public Rigidbody2D _rb;

    public Animator _anim;

    public float moveSpeed = 5f;
    private Vector2 moveInput;

    public Transform _gunArm;

    private Camera theCam;

    public GameObject _bulletToFire;
    public Transform _firePoint;

    public float timeBetweenShots=0.2f;
    private float shotCounter;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _anim = GetComponent<Animator>();
        theCam = Camera.main;
    }

   
    void Update()
    {
        moveInput.x = Input.GetAxisRaw("Horizontal");
        moveInput.y = Input.GetAxisRaw("Vertical");
        moveInput.Normalize();

      //  transform.position += new Vector3(moveInput.x*Time.deltaTime*moveSpeed, moveInput.y*moveSpeed*Time.deltaTime);

        _rb.velocity = new Vector2(moveInput.x*moveSpeed, moveInput.y*moveSpeed);

        Vector3 mousePos = Input.mousePosition;

        Vector3 screenPoint = theCam.WorldToScreenPoint(transform.localPosition);

        if (mousePos.x < screenPoint.x)
        {
            transform.localScale = new Vector3(-1f, 1f, 1f);
            _gunArm.localScale = new Vector3(-1f, -1f, 1f);
        }
        else
        {
            transform.localScale = Vector3.one;
            _gunArm.localScale = Vector3.one;
        }

        Vector3 offset = new Vector2(mousePos.x - screenPoint.x, mousePos.y - screenPoint.y);
        float angle = Mathf.Atan2(offset.y, offset.x) * Mathf.Rad2Deg;
        _gunArm.rotation = Quaternion.Euler(0, 0, angle);


        if (Input.GetMouseButtonDown(0))
        {
            Instantiate(_bulletToFire, _firePoint.position, _firePoint.rotation);
            shotCounter = timeBetweenShots;
        }

        if (Input.GetMouseButton(0))
        {
            shotCounter -= Time.deltaTime;

            if (shotCounter <= 0)
            {
                Instantiate(_bulletToFire, _firePoint.position, _firePoint.rotation);

                shotCounter = timeBetweenShots;
            }
        }





        if (moveInput != Vector2.zero)
        {
            _anim.SetBool("isMoving", true);
        }
        else
        {
            _anim.SetBool("isMoving", false);
        }
    }
}
