﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunPickUp : MonoBehaviour
{
    public Gun theGun;

    public float waitforBeCollected = .5f;

    void Start()
    {

    }


    void Update()
    {
        if (waitforBeCollected > 0)
        {
            waitforBeCollected -= Time.deltaTime;
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player" && waitforBeCollected <= 0)
        {
            bool hasGun = false;

            foreach(Gun gunToCheck in PlayerController.Instance.avalibleGuns)
            {
                if(theGun.weaponName == gunToCheck.weaponName)
                {
                    hasGun = true;
                }
            }

            if (!hasGun)
            {
                Gun theClone = Instantiate(theGun);
                theClone.transform.parent = PlayerController.Instance._gunArm;
                theClone.transform.position = PlayerController.Instance._gunArm.position;
                theClone.transform.localRotation = Quaternion.Euler(Vector3.zero);
                theClone.transform.localScale = Vector3.one;

                PlayerController.Instance.avalibleGuns.Add(theClone);
                PlayerController.Instance.currentGun= PlayerController.Instance.avalibleGuns.Count-1;
                PlayerController.Instance.SwitchGun();
            }


            Destroy(gameObject);

            AudioManager.Instance.PlaySFX(7);
        }
    }
}
