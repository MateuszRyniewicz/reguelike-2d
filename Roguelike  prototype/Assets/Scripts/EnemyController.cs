﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public SpriteRenderer _body;
    public Rigidbody2D _rb;
    private Animator _anim;

    public float _speedEnemy;
    public int health = 150;

    [Header("GameObject")]
    public GameObject[] _deathSplatters;
    public GameObject hitEffect;

    [Header("Shoting")]
    public float shotRange;
    public bool shouldShot;
    public GameObject bullet;
    public Transform firePoint;
    public float fireRate=0.2f;
    private float fireCounter;



    [Header("Patroling")]
    public bool shouldPatrol;
    public Transform[] patrolPoints;
    private int curretPatrolPoint;



    [Header("Chaseing")]
    public bool shouldChasePlayer;
    public float _rangeToChasePlayer;
    private Vector3 moveDirection;

    [Header("Runing")]
    public bool shouldRunAway;
    public float runAwayRunRange;

    [Header("Wondering")]
    public bool shouldWander;
    public float wanderLength,pauseLength;
    private float wanderCounter, pauseCounter;
    private Vector3 wanderDirection;

    [Header("EnemyPickUp")]
    public bool shouldDropItem;
    public GameObject[] itemToDrop;
    public float itemDropPercent;

   
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _anim = GetComponent<Animator>();

        if (shouldWander)
        {
            pauseCounter = Random.Range(pauseLength*0.75f, pauseLength*1.25f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_body.isVisible && PlayerController.Instance.gameObject.activeInHierarchy)
        {
            moveDirection = Vector3.zero;

            if (Vector3.Distance(transform.position, PlayerController.Instance.transform.position) < _rangeToChasePlayer && shouldChasePlayer)
            {
                moveDirection = PlayerController.Instance.transform.position - transform.position;
            }
            else
            {
                if (shouldWander)
                {
                    if (wanderCounter > 0)
                    {
                        wanderCounter -= Time.deltaTime;

                        moveDirection = wanderDirection;

                        if (wanderCounter <= 0)
                        {
                            pauseCounter = Random.Range(pauseLength * 0.75f, pauseLength * 1.25f);
                        }
                    }
                    if (pauseCounter > 0)
                    {
                        pauseCounter -= Time.deltaTime;

                        if (pauseCounter <= 0)
                        {
                            wanderCounter = Random.Range(wanderLength * 0.75f, wanderLength * 1.25f);

                            wanderDirection = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0f);
                        }
                    }
                }

                if (shouldPatrol)
                {
                    moveDirection = patrolPoints[curretPatrolPoint].position - transform.position;

                    if (Vector3.Distance(transform.position , patrolPoints[curretPatrolPoint].position) < 0.2f)
                    {
                        curretPatrolPoint++;

                        if (curretPatrolPoint >= patrolPoints.Length)
                        {
                            curretPatrolPoint = 0;
                        }
                    }
                }
            }

            if(shouldRunAway && Vector3.Distance(transform.position, PlayerController.Instance.transform.position) < runAwayRunRange)
            {
                moveDirection = transform.position - PlayerController.Instance.transform.position;
            }

          /*else
            {
                moveDirection = Vector3.zero;
            }
            */
            moveDirection.Normalize();

            _rb.velocity = moveDirection * _speedEnemy;

            if (shouldShot && Vector3.Distance(transform.position, PlayerController.Instance.transform.position)<shotRange)
            {
                fireCounter -= Time.deltaTime;

                if (fireCounter <= 0)
                {
                    fireCounter = fireRate;
                    Instantiate(bullet, firePoint.position, firePoint.rotation);
                    AudioManager.Instance.PlaySFX(13);
                }
            }

        }
        else
        {
            _rb.velocity = Vector3.zero;
        }



        if(moveDirection!= Vector3.zero)
        {
            _anim.SetBool("isMoving", true);
        }
        else
        {
            _anim.SetBool("isMoving", false);
        }

            
    }

    public void DamageEnemy(int damage)
    {
        health -= damage;
        AudioManager.Instance.PlaySFX(2);

        Instantiate(hitEffect, transform.position, Quaternion.identity);

        if (health <= 0)
        {
            Destroy(gameObject);
            AudioManager.Instance.PlaySFX(1);

            int selletedSplatters =Random.Range(0,_deathSplatters.Length);
            int selletedRotation = Random.Range(0, 4);

            Instantiate(_deathSplatters[selletedSplatters], transform.position, Quaternion.Euler(0f,0f,selletedRotation*90));

            //  Instantiate(_deathSplatter, transform.position, Quaternion.identity);
            
            //drop items
            if (shouldDropItem)
            {
                float dropChance = Random.Range(0f, 100f);

                if (dropChance < itemDropPercent)
                {
                    int randomItem = Random.Range(0, itemToDrop.Length);

                    Instantiate(itemToDrop[randomItem], transform.position, transform.rotation);
                }
            }

        }
    }

   
}
