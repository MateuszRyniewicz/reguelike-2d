﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockCharacterCage : MonoBehaviour
{

    private bool canUnlock;
    public GameObject message;

    public CharacterSelector[] charSelecte;
    CharacterSelector playerToUnlock;

    public SpriteRenderer cagedSR;
    // Start is called before the first frame update
    void Start()
    {
        playerToUnlock = charSelecte[Random.Range(0, charSelecte.Length)];

        cagedSR.sprite = playerToUnlock.playerToSpawn.bodySR.sprite;
    }

    // Update is called once per frame
    void Update()
    {
        if (canUnlock)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                PlayerPrefs.SetInt(playerToUnlock.playerToSpawn.name, 1);
                Instantiate(playerToUnlock, transform.position, transform.rotation);

                gameObject.SetActive(false);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            canUnlock = true;
            message.gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.tag== "Player")
        {
            canUnlock = false;
            message.gameObject.SetActive(false);
        }
    }
}
