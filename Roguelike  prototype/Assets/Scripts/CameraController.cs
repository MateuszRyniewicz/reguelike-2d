﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public static CameraController Instance;

    public float moveSpeed=30f;
    public Transform target;

    public Camera mainCamera;
    public Camera bigMapCamera;

    private bool bigMapActive;

    public bool isBossRoom;


    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        if (isBossRoom)
        {
            target = PlayerController.Instance.transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(target.position.x, target.position.y, transform.position.z),moveSpeed*Time.deltaTime);

        }

        if (Input.GetKeyDown(KeyCode.M) &&!isBossRoom)
        {
            if (!bigMapActive)
            {
                ActivateBigMap();
            }
            else
            {
                DeactivateBigMap();
            }
        }
      

    }

    public void ChangeTarget(Transform newTarget)
    {
        target = newTarget;
    }

    public void ActivateBigMap()
    {
        if (!LevelManager.Instance.isPause)
        {

            bigMapActive = true;

            bigMapCamera.enabled = true;
            mainCamera.enabled = false;

            PlayerController.Instance.canMove = false;

            Time.timeScale = 0f;

            UIController.Instance.mapDisplay.SetActive(false);
            UIController.Instance.BigMapText.SetActive(true);

        }
    }

    public void DeactivateBigMap()
    {
        if (!LevelManager.Instance.isPause)
        {

            bigMapActive = false;

            bigMapCamera.enabled = false;
            mainCamera.enabled = true;

            PlayerController.Instance.canMove = true;

            Time.timeScale = 1f;

            UIController.Instance.mapDisplay.SetActive(true);
            UIController.Instance.BigMapText.SetActive(false);
        }
    }
}
