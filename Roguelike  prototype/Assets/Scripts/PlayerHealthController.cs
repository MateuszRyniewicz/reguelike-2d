﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthController : MonoBehaviour
{
    public static PlayerHealthController Instance;

    public int curretHealth;
    public int maxHealth=5;

    public float damageInvincLength=1f;
    private float invincCount;



    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
           
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        curretHealth = CharacterTracker.instance.currentHealth;
        maxHealth = CharacterTracker.instance.maxHealth;




       // curretHealth = maxHealth;
        UIController.Instance.healthSlider.maxValue = maxHealth;
        UIController.Instance.healthSlider.value = curretHealth;
        UIController.Instance.healthText.text = curretHealth.ToString() + " / " + maxHealth.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (invincCount > 0)
        {
            invincCount -= Time.deltaTime;

            if (invincCount <= 0)
            {
                PlayerController.Instance.bodySR.color = new Color(PlayerController.Instance.bodySR.color.r, PlayerController.Instance.bodySR.color.g, PlayerController.Instance.bodySR.color.b, 1f);
            }
        }   
    }

    public void DamagePlayer()
    {
        if (invincCount <= 0)
        {


            AudioManager.Instance.PlaySFX(11);
            curretHealth--;
            invincCount = damageInvincLength;

            PlayerController.Instance.bodySR.color = new Color(PlayerController.Instance.bodySR.color.r, PlayerController.Instance.bodySR.color.g, PlayerController.Instance.bodySR.color.b, 0.5f);

            if (curretHealth <= 0)
            {
                PlayerController.Instance.gameObject.SetActive(false);

                UIController.Instance.deathScreen.SetActive(true);

                AudioManager.Instance.GamePlayOver();
                AudioManager.Instance.PlaySFX(8);
            }




            UIController.Instance.healthSlider.value = curretHealth;
            UIController.Instance.healthText.text = curretHealth.ToString() + " / " + maxHealth.ToString();
        }
    }

    public void MakeInvincible(float lenght)
    {
        invincCount = lenght;
        PlayerController.Instance.bodySR.color = new Color(PlayerController.Instance.bodySR.color.r, PlayerController.Instance.bodySR.color.g, PlayerController.Instance.bodySR.color.b, 0.5f);
    }

    public void HealPlayer(int healhAmount)
    {
        curretHealth += healhAmount;
        if (curretHealth > maxHealth)
        {
            curretHealth = maxHealth;
        }

        UIController.Instance.healthSlider.value = curretHealth;
        UIController.Instance.healthText.text = curretHealth.ToString() + " / " + maxHealth.ToString();
    }

    public void IncreaseMaxHealth(int amount)
    {
        maxHealth += amount;
        curretHealth = maxHealth;

        UIController.Instance.healthSlider.maxValue = maxHealth;
        UIController.Instance.healthSlider.value = curretHealth;
        UIController.Instance.healthText.text = curretHealth.ToString() + " / " + maxHealth.ToString(); 
    }
}
