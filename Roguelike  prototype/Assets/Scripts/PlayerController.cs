﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;

    public Rigidbody2D _rb;

    public Animator _anim;

    public float moveSpeed = 5f;
    private Vector2 moveInput;

    public Transform _gunArm;

  //  private Camera theCam;

   /* public GameObject _bulletToFire;
    public Transform _firePoint;

    public float timeBetweenShots=0.2f;
    private float shotCounter;

    */
    public SpriteRenderer bodySR;

    private float _activeMovieSpeed;
    public float dashSpeed = 8f, dashLenght = .5f, dashCooldown = 1f, dashInvinvibility=.5f;
    [HideInInspector]
    public float dashCounter;
    private float dashCoolCounter;

    [HideInInspector]
    public bool canMove=true;

    public List<Gun> avalibleGuns = new List<Gun>();
    [HideInInspector]
    public int currentGun;

    private void Awake()
    {
      
            Instance = this;
            DontDestroyOnLoad(this);
        
    }

    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _anim = GetComponent<Animator>();
       // theCam = Camera.main;

        _activeMovieSpeed = moveSpeed; 
    }


    void Update()
    {
        if (canMove && !LevelManager.Instance.isPause)
        {


            moveInput.x = Input.GetAxisRaw("Horizontal");
            moveInput.y = Input.GetAxisRaw("Vertical");
            moveInput.Normalize();

            //  transform.position += new Vector3(moveInput.x*Time.deltaTime*moveSpeed, moveInput.y*moveSpeed*Time.deltaTime);

            _rb.velocity = moveInput * _activeMovieSpeed;

            Vector3 mousePos = Input.mousePosition;

            Vector3 screenPoint = CameraController.Instance.mainCamera.WorldToScreenPoint(transform.localPosition);

            if (mousePos.x < screenPoint.x)
            {
                transform.localScale = new Vector3(-1f, 1f, 1f);
                _gunArm.localScale = new Vector3(-1f, -1f, 1f);
            }
            else
            {
                transform.localScale = Vector3.one;
                _gunArm.localScale = Vector3.one;
            }

            Vector3 offset = new Vector2(mousePos.x - screenPoint.x, mousePos.y - screenPoint.y);
            float angle = Mathf.Atan2(offset.y, offset.x) * Mathf.Rad2Deg;
            _gunArm.rotation = Quaternion.Euler(0, 0, angle);


            /* if (Input.GetMouseButtonDown(0))
             {
                 Instantiate(_bulletToFire, _firePoint.position, _firePoint.rotation);
                 shotCounter = timeBetweenShots;
                 AudioManager.Instance.PlaySFX(12);
             }

             if (Input.GetMouseButton(0))
             {
                 shotCounter -= Time.deltaTime;

                 if (shotCounter <= 0)
                 {
                     Instantiate(_bulletToFire, _firePoint.position, _firePoint.rotation);

                     AudioManager.Instance.PlaySFX(12);

                     shotCounter = timeBetweenShots;
                 }
             }
             */

            if (Input.GetKeyDown(KeyCode.Tab))
            {
                if (avalibleGuns.Count > 0)
                {
                    currentGun++;
                    if (currentGun >= avalibleGuns.Count)
                    {
                        currentGun = 0;
                    }

                    SwitchGun();
                }
                else
                {
                    Debug.LogError("Player has no guns");
                }

            }


            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (dashCoolCounter <= 0 && dashCounter <= 0)
                {
                    _activeMovieSpeed = dashSpeed;
                    dashCounter = dashLenght;
                    _anim.SetTrigger("dash");

                    PlayerHealthController.Instance.MakeInvincible(dashInvinvibility);

                    AudioManager.Instance.PlaySFX(8);
                }
            }

            if (dashCounter > 0)
            {
                dashCounter -= Time.deltaTime;
                if (dashCounter <= 0)
                {
                    _activeMovieSpeed = moveSpeed;
                    dashCoolCounter = dashCooldown;
                }
            }

            if (dashCoolCounter > 0)
            {
                dashCoolCounter -= Time.deltaTime;
            }











            if (moveInput != Vector2.zero)
            {
                _anim.SetBool("isMoving", true);
            }
            else
            {
                _anim.SetBool("isMoving", false);
            }
        }
        else
        {
            _rb.velocity = Vector3.zero;
            _anim.SetBool("isMoving", false);
        }
    }

    public void SwitchGun()
    {
        foreach(Gun theGun in avalibleGuns)
        {
            theGun.gameObject.SetActive(false);
        }

        avalibleGuns[currentGun].gameObject.SetActive(true);

        UIController.Instance.currentGun.sprite = avalibleGuns[currentGun].gunUI;
        UIController.Instance.gunText.text = avalibleGuns[currentGun].weaponName;
    }
}
