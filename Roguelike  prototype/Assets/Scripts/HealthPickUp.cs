﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickUp : MonoBehaviour
{
    public int healthAmount=1;

    public float waitforBeCollected = .5f;
   
    void Start()
    {
        
    }

  
    void Update()
    {
        if (waitforBeCollected > 0)
        {
            waitforBeCollected -= Time.deltaTime;
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player" && waitforBeCollected<=0)
        {
            PlayerHealthController.Instance.HealPlayer(healthAmount);

            Destroy(gameObject);
            AudioManager.Instance.PlaySFX(7);
        }   
    }
}
