﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelGenerator : MonoBehaviour
{
    public GameObject layoutRoom;
    public Color startColor, endColor,shopColor,gunRoomColor;

    public int distanceToEnd;
    public bool includeShop;
    public int minDistanceToShop, maxDiscanceToShop;

    public bool includeGunRoom;
    public int minDistanceToGunRoom, maxDistanceToGunRoom;

    public Transform generatorPoint;

    public enum Directions { up,right,down,left};
    public Directions SelectedDirection;

    public float xOffset=18f, yOffset = 10f;

    public LayerMask whatIsRoom;

    private GameObject endRoom,shopRoom, gunRoom;

    public List<GameObject> LayoutRoomGameObjects = new List<GameObject>();

    public RoomPrefabs rooms;

    public List<GameObject> GeneratorOutLines = new List<GameObject>();

    public RoomCenter centerStart, centerEnd,centerShop, centerGunRoom;

    public RoomCenter[] potencialCenter;

    // Start is called before the first frame update
    void Start()
    {
        Instantiate(layoutRoom, generatorPoint.position, generatorPoint.rotation).GetComponent<SpriteRenderer>().color = startColor;

        SelectedDirection =(Directions)Random.Range(0, 4);
        MoveGenerationPoint();

        for(int i=0; i < distanceToEnd; i++)
        {
            GameObject newRoom= Instantiate(layoutRoom, generatorPoint.position, generatorPoint.rotation);

            LayoutRoomGameObjects.Add(newRoom);

            if (i + 1 == distanceToEnd)
            {
                newRoom.GetComponent<SpriteRenderer>().color = endColor;
                LayoutRoomGameObjects.RemoveAt(LayoutRoomGameObjects.Count - 1);
                endRoom = newRoom;
            }

            SelectedDirection = (Directions)Random.Range(0, 4);
            MoveGenerationPoint();

            while (Physics2D.OverlapCircle(generatorPoint.position, 0.2f, whatIsRoom))
            {
                MoveGenerationPoint();
            }
        }

        if (includeShop)
        {
            int shopSelector = Random.Range(minDistanceToShop, maxDiscanceToShop + 1);
            shopRoom = LayoutRoomGameObjects[shopSelector];
            LayoutRoomGameObjects.RemoveAt(shopSelector);
            shopRoom.GetComponent<SpriteRenderer>().color = shopColor;
        }
        if (includeGunRoom)
        {
            int gunSelector = Random.Range(minDistanceToGunRoom, maxDiscanceToShop + 1);
            gunRoom = LayoutRoomGameObjects[gunSelector];
            LayoutRoomGameObjects.RemoveAt(gunSelector);
            shopRoom.GetComponent<SpriteRenderer>().color = gunRoomColor;
        }

        ///create Loop outlines

        CreateRoomOutline(Vector3.zero);
        foreach(GameObject room in LayoutRoomGameObjects)
        {

            CreateRoomOutline(room.transform.position);
        }
        CreateRoomOutline(endRoom.transform.position);

        if (includeShop)
        {
            CreateRoomOutline(shopRoom.transform.position);
        }
        if (includeGunRoom)
        {
            CreateRoomOutline(gunRoom.transform.position);
        }


        foreach(GameObject outlines in GeneratorOutLines)
        {
            bool generateCenter = true;

            if (outlines.transform.position == Vector3.zero)
            {
                Instantiate(centerStart, outlines.transform.position, transform.rotation).theRoom = outlines.GetComponent<Room>();

                generateCenter = false;
            }

            if (outlines.transform.position == endRoom.transform.position)
            {
                Instantiate(centerEnd, outlines.transform.position, transform.rotation).theRoom = outlines.GetComponent<Room>();

                generateCenter = false;

            }

            if (includeShop)
            {
                if (outlines.transform.position == shopRoom.transform.position)
                {
                    Instantiate(centerShop, outlines.transform.position, transform.rotation).theRoom = outlines.GetComponent<Room>();

                    generateCenter = false;

                }
            }
            if (includeGunRoom)
            {
                if (outlines.transform.position == gunRoom.transform.position)
                {
                    Instantiate(centerGunRoom, outlines.transform.position, transform.rotation).theRoom = outlines.GetComponent<Room>();

                    generateCenter = false;
                }
            }

           



            if (generateCenter)
            {
                int centerSelect = Random.Range(0, potencialCenter.Length);

                Instantiate(potencialCenter[centerSelect], outlines.transform.position, transform.rotation).theRoom = outlines.GetComponent<Room>();

                
            }
            
        }
    }

    // Update is called once per frame
    void Update()
    {

#if UNITY_EDITOR
        if (Input.GetKey(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
#endif

    }

    public void MoveGenerationPoint()
    {
        switch (SelectedDirection)
        {
            case
                Directions.up:
                generatorPoint.position += new Vector3(0, yOffset, 0);
                break;
            case
                Directions.right:
                generatorPoint.position += new Vector3(xOffset, 0, 0);
                break;
            case
                Directions.down:
                generatorPoint.position += new Vector3(0, -yOffset, 0);
                break;
            case
                Directions.left:
                generatorPoint.position += new Vector3(-xOffset, 0, 0);
                break;
                
        }
            
    }

    public void CreateRoomOutline(Vector3 roomPosition)
    {
        bool roomAbove = Physics2D.OverlapCircle(roomPosition + new Vector3(0, yOffset, 0), 0.2f,whatIsRoom);
        bool roomBelow = Physics2D.OverlapCircle(roomPosition + new Vector3(0, -yOffset, 0), 0.2f, whatIsRoom);
        bool roomLeft =  Physics2D.OverlapCircle(roomPosition + new Vector3(-xOffset, 0, 0), 0.2f, whatIsRoom);
        bool roomRight = Physics2D.OverlapCircle(roomPosition + new Vector3(xOffset, 0, 0), 0.2f, whatIsRoom);

        int directionCount = 0;
        if (roomAbove)
        {
            directionCount++;
        }
        if (roomBelow)
        {
            directionCount++;
        }
        if (roomLeft)
        {
            directionCount++;
        }
        if (roomRight)
        {
            directionCount++;
        }

        switch (directionCount)
        {
            case 0:
                Debug.LogError("Found no room exists");
                break;
            case 1:
                if (roomAbove)
                {
                  GeneratorOutLines.Add(Instantiate(rooms.singleUp,roomPosition,transform.rotation));
                }
                if (roomBelow)
                {
                    GeneratorOutLines.Add(Instantiate(rooms.singleDown, roomPosition, transform.rotation));
                }
                if (roomLeft)
                {
                    GeneratorOutLines.Add(Instantiate(rooms.singleLeft, roomPosition, transform.rotation));
                }
                if (roomRight)
                {
                    GeneratorOutLines.Add(Instantiate(rooms.singleRight, roomPosition, transform.rotation));
                }
                break;
            case 2:
                if(roomAbove && roomBelow)
                {
                    GeneratorOutLines.Add(Instantiate(rooms.doubleUpDown, roomPosition, transform.rotation));
                }

                if(roomLeft && roomRight)
                {
                    GeneratorOutLines.Add(Instantiate(rooms.doubleLeftRight, roomPosition, transform.rotation));
                }
                if(roomAbove && roomRight)
                {
                    GeneratorOutLines.Add(Instantiate(rooms.doubleUpRight, roomPosition, transform.rotation));
                }
                if (roomBelow && roomRight)
                {
                    GeneratorOutLines.Add(Instantiate(rooms.doubleRightDown, roomPosition, transform.rotation));
                }
                if(roomBelow && roomLeft)
                {
                    GeneratorOutLines.Add(Instantiate(rooms.doubleDownLeft,roomPosition,transform.rotation));
                }
                if(roomAbove && roomLeft)
                {
                    GeneratorOutLines.Add(Instantiate(rooms.doubleLeftUp, roomPosition, transform.rotation));
                }
                break;
            case 3:
                if(roomAbove && roomRight && roomBelow)
                {
                    GeneratorOutLines.Add(Instantiate(rooms.tripleUpRightDown, roomPosition, transform.rotation));
                }
                if(roomAbove && roomLeft && roomBelow)
                {
                    GeneratorOutLines.Add(Instantiate(rooms.tripleDownLeftUp, roomPosition, transform.rotation));
                }
                if(roomRight && roomBelow && roomLeft)
                {
                    GeneratorOutLines.Add(Instantiate(rooms.tripleRightDownLeft,roomPosition,transform.rotation));
                }
                if(roomRight && roomAbove && roomLeft)
                {
                    GeneratorOutLines.Add(Instantiate(rooms.tripleLeftUpRight, roomPosition, transform.rotation));
                }
                break;


            case 4:
                if(roomBelow && roomAbove && roomLeft && roomRight)
                {
                    GeneratorOutLines.Add(Instantiate(rooms.fourway, roomPosition, transform.rotation));
                }
                break;

        }
    }
}
[System.Serializable]
 public class RoomPrefabs
 {
    public GameObject singleUp, singleDown, singleRight, singleLeft,
        doubleUpDown, doubleLeftRight, doubleUpRight, doubleRightDown, doubleDownLeft, doubleLeftUp,
        tripleUpRightDown,tripleRightDownLeft,tripleDownLeftUp,tripleLeftUpRight,
        fourway;
        
 }
