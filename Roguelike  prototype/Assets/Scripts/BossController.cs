﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    public static BossController instance;


    public BossAction[] atctions;
    private int currentAction;
    public float actionCounter;

    public float shotCounter;
    public Vector2 moveDicetion;
    private Rigidbody2D rb;


    public int curretHealth;

    public GameObject deathEffect, hitEffect;
    public GameObject levelExit;

    public BossSequence[] sequences;
    public int currentSequence;


    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        atctions = sequences[currentSequence].actions;

        rb = GetComponent<Rigidbody2D>();

        actionCounter = atctions[currentAction].actionLenght;

        UIController.Instance.bossHealthBar.maxValue = curretHealth;
        UIController.Instance.bossHealthBar.value = curretHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if (actionCounter > 0)
        {
            actionCounter -= Time.deltaTime;



            // handle movement
            moveDicetion = Vector2.zero;


            if (atctions[currentAction].shouldMove)
            {
                if (atctions[currentAction].shouldChasePlayer)
                {
                    moveDicetion = PlayerController.Instance.transform.position - transform.position;
                    moveDicetion.Normalize();
                }

                if (atctions[currentAction].moveToPoint && Vector3.Distance(transform.position , atctions[currentAction].pointToMoveTo.position) > 0.5f)
                {
                    moveDicetion = atctions[currentAction].pointToMoveTo.transform.position - transform.position;
                    moveDicetion.Normalize();
                }
            }





            rb.velocity = moveDicetion * atctions[currentAction].moveSpeed;



            // handle shooting
            if (atctions[currentAction].shouldShoot)
            {
                shotCounter -= Time.deltaTime;
                if (shotCounter <= 0)
                {
                    shotCounter = atctions[currentAction].timeBetweenShoot;

                    foreach(Transform t in atctions[currentAction].shotPoints)
                    {
                        Instantiate(atctions[currentAction].itemToShoot, t.position, t.rotation);
                    }
                }
            }
        }
        else
        {
            currentAction++;

            if(currentAction > atctions.Length)
            {
                currentAction = 0;
            }

            actionCounter = atctions[currentAction].actionLenght;
        }
    }


    public void TakeDamage(int damageAmount)
    {
        curretHealth -= damageAmount;

        if (curretHealth <= 0)
        {
            gameObject.SetActive(false);

            Instantiate(deathEffect, transform.position, transform.rotation);

            if (Vector3.Distance(PlayerController.Instance.transform.position, levelExit.transform.position) < 2f) 
            {
                levelExit.transform.position += new Vector3(4f, 0, 0);
            }

            levelExit.SetActive(true);

            UIController.Instance.bossHealthBar.gameObject.SetActive(false);
        }
        else
        {
            if(curretHealth<=sequences[currentSequence].endSequneceHealth && currentSequence < sequences.Length - 1)
            {
                currentSequence++;
                atctions = sequences[currentSequence].actions;
                currentAction = 0;
                actionCounter = atctions[currentAction].actionLenght;
            }
        }

        UIController.Instance.bossHealthBar.value = curretHealth;
    }
}
[System.Serializable]
public class BossAction
{
    [Header("Action")]
    public float actionLenght;

    public bool shouldMove;
    public bool shouldChasePlayer;
    public float moveSpeed;
    public bool moveToPoint;
    public Transform pointToMoveTo;


    public bool shouldShoot;
    public GameObject itemToShoot;
    public float timeBetweenShoot;
    public Transform[] shotPoints;
}
[System.Serializable]
public class BossSequence
{
    [Header("Sequences")]
    public BossAction[] actions;

    public int endSequneceHealth;
}
