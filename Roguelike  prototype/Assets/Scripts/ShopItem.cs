﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItem : MonoBehaviour
{
    public GameObject buyMassage;

    private bool inBuyZone;

    public bool isHealthRestore, isHealthUpgrade, isWeapon;

    public int ItemCost;

    public int healthUpgradeAmount;

    public Gun[] potencialGuns;
    private Gun theGun;
    public SpriteRenderer gunSprite;
    public Text infoText;

    // Start is called before the first frame update
    void Start()
    {
        if (isWeapon)
        {
            int selectedGun = Random.Range(0, potencialGuns.Length);
            theGun = potencialGuns[selectedGun];

            gunSprite.sprite = theGun.gunShopSprite;
            infoText.text = theGun.weaponName + "\n - " + theGun.itemCost + " Gold - ";
            ItemCost = theGun.itemCost;
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (inBuyZone)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (LevelManager.Instance.currentCoints >= ItemCost)
                {
                    LevelManager.Instance.SpendCoints(ItemCost);

                    if (isHealthRestore)
                    {
                        PlayerHealthController.Instance.HealPlayer(PlayerHealthController.Instance.maxHealth);
                    }

                    if (isHealthUpgrade)
                    {
                        PlayerHealthController.Instance.IncreaseMaxHealth(healthUpgradeAmount);
                    }
                    if (isWeapon)
                    {
                        Gun theClone = Instantiate(theGun);
                        theClone.transform.parent = PlayerController.Instance._gunArm;
                        theClone.transform.position = PlayerController.Instance._gunArm.position;
                        theClone.transform.localRotation = Quaternion.Euler(Vector3.zero);
                        theClone.transform.localScale = Vector3.one;

                        PlayerController.Instance.avalibleGuns.Add(theClone);
                        PlayerController.Instance.currentGun = PlayerController.Instance.avalibleGuns.Count - 1;
                        PlayerController.Instance.SwitchGun();
                    }


                    gameObject.SetActive(false);
                    inBuyZone = false;

                    AudioManager.Instance.PlaySFX(18);
                }
                else
                {
                    AudioManager.Instance.PlaySFX(19);
                }

            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            buyMassage.SetActive(true);

            inBuyZone = true;
        }

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            buyMassage.SetActive(false);

            inBuyZone = false;
        }
    }
}
