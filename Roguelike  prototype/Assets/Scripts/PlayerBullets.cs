﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullets : MonoBehaviour
{
    Rigidbody2D _rb;

    public float _speedBullet = 7.5f;
    public GameObject _impactEffect;
    public int _damageToGive = 50;
    
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

   
    void Update()
    {
        _rb.velocity = transform.right * _speedBullet;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        Instantiate(_impactEffect, transform.position, transform.rotation);
        Destroy(gameObject);

        AudioManager.Instance.PlaySFX(4);

        if (other.tag == "Enemy")
        {

        other.GetComponent<EnemyController>().DamageEnemy(_damageToGive);

        }
        if (other.tag == "Boss")
        {
           BossController.instance.TakeDamage(_damageToGive);

            Instantiate(BossController.instance.hitEffect, transform.position, transform.rotation);
        }

    }

    public void OnBecameInvisible()
    {
        Destroy(this.gameObject); 
    }
}
