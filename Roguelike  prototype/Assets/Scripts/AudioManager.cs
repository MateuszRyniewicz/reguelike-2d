﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public static AudioManager Instance;

    public AudioSource levelMusic, gameOverMusic, WinMusic;

    public AudioSource[] sfx;


    public void Awake()
    {
        Instance = this;
        
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GamePlayOver()
    {
        levelMusic.Stop();

        gameOverMusic.Play();
    }
    public void PlayLevelWin()
    {
        levelMusic.Stop();

        WinMusic.Play();
    }

    public void PlaySFX(int sfxToPlay)
    {
        sfx[sfxToPlay].Stop();
        sfx[sfxToPlay].Play();
    }
}
