﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteSortOlder : MonoBehaviour
{
    private  SpriteRenderer _sr;
    // Start is called before the first frame update
    void Start()
    {
        _sr = GetComponent<SpriteRenderer>();
        _sr.sortingOrder = Mathf.RoundToInt(transform.position.y * -10f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
