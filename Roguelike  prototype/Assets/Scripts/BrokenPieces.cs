﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrokenPieces : MonoBehaviour
{

    public float moveSpeed=3;
    private Vector3 moveDirection;

    public float deceleration= 5f;

    public float liveTime = 3f;
    public SpriteRenderer _sp;
    public float fadeSpeed = 2.5f;

    // Start is called before the first frame update
    void Start()
    {
        moveDirection.x = Random.Range(-moveSpeed, moveSpeed);
        moveDirection.y = Random.Range(-moveSpeed, moveSpeed);
        _sp = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += moveDirection * Time.deltaTime;

        moveDirection = Vector3.Lerp(moveDirection, Vector3.zero, deceleration * Time.deltaTime);

        liveTime -= Time.deltaTime;

        if (liveTime < 0)
        {

            _sp.color = new Color(_sp.color.r, _sp.color.g, _sp.color.b, Mathf.MoveTowards(_sp.color.a, 0f, fadeSpeed*Time.deltaTime));

            if (_sp.color.a == 0)
            {
                Destroy(gameObject);

            }

        }
    }
}
