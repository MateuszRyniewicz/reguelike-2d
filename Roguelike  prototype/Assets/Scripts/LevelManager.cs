﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;
    public float waitToLoad = 4.5f;

    public string nextLevel;

    public bool isPause;

    public int currentCoints;

    public Transform startPoint;


    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        PlayerController.Instance.transform.position = startPoint.position;
        PlayerController.Instance.canMove = true;

        currentCoints = CharacterTracker.instance.currentCoins;

        Time.timeScale = 1f;

        UIController.Instance.coinText.text = currentCoints.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PouseUnPouse();
        }
    }

    public IEnumerator LevelEnd()
    {
        AudioManager.Instance.PlayLevelWin();
        PlayerController.Instance.canMove = false;
        UIController.Instance.StartFideToBlack();
        yield return new WaitForSeconds(waitToLoad);

        CharacterTracker.instance.currentCoins = currentCoints;
        CharacterTracker.instance.currentHealth = PlayerHealthController.Instance.curretHealth;
        CharacterTracker.instance.maxHealth = PlayerHealthController.Instance.maxHealth;
        

        SceneManager.LoadScene(nextLevel);
    }

    public void PouseUnPouse()
    {
        if (!isPause)
        {
            UIController.Instance.pauseMenu.SetActive(true);

            isPause = true;
            Time.timeScale = 0f;
        }
        else
        {
            UIController.Instance.pauseMenu.SetActive(false);

            isPause = false;
            Time.timeScale = 1f;
        }

    }

    public void GetCoints(int amount)
    {
        currentCoints += amount;

        UIController.Instance.coinText.text = currentCoints.ToString();
    }
    
    public void SpendCoints(int amount)
    {
        currentCoints -= amount;

        if (currentCoints < 0)
        {
            currentCoints = 0;
        }

        UIController.Instance.coinText.text = currentCoints.ToString();
    }
}
