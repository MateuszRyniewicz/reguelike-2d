﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public GameObject _bulletToFire;
    public Transform _firePoint;

    public float timeBetweenShots = 0.2f;
    private float shotCounter;

    public string weaponName;
    public Sprite gunUI;


    public int itemCost;
    public Sprite gunShopSprite;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerController.Instance.canMove && !LevelManager.Instance.isPause)
        {
            if (shotCounter > 0)
            {
                shotCounter -= Time.deltaTime;
            }
            else
            {

                if (Input.GetMouseButtonDown(0) || Input.GetMouseButton(0))
                {
                    Instantiate(_bulletToFire, _firePoint.position, _firePoint.rotation);
                    shotCounter = timeBetweenShots;
                    AudioManager.Instance.PlaySFX(12);
                }

              /*  if (Input.GetMouseButton(0))
                {
                    shotCounter -= Time.deltaTime;

                    if (shotCounter <= 0)
                    {
                        Instantiate(_bulletToFire, _firePoint.position, _firePoint.rotation);

                        AudioManager.Instance.PlaySFX(12);

                        shotCounter = timeBetweenShots;
                    }
                }
                */
            }
        }
    }
}
